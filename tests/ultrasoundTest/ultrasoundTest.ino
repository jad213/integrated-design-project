#define trigPin 0
#define echoPin 1

double readUltrasound()
{
	long duration, distance;
	digitalWrite(trigPin, LOW);
	delayMicroseconds(2);
	digitalWrite(trigPin, HIGH);
	delayMicroseconds(10);
	duration = pulseIn(echoPin, HIGH);
	distance = duration / 2 / 29.1 * 10;
	Serial.println(distance);
}

void setup() {
  // put your setup code here, to run once:
	// connect to serial
	Serial.begin(9600);
	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
	delay(1000);
	readUltrasound();
}
