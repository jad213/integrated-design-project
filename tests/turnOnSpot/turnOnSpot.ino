// copied and edited from DCMotorTest

#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor *leftMotor = AFMS.getMotor(2);

// You can also make another motor on port M2
//Adafruit_DCMotor *myOtherMotor = AFMS.getMotor(2);

void turn(int motorSpeed, bool rightIsTrue)
{
  // set motor speeds
  rightMotor->setSpeed(motorSpeed);
  leftMotor->setSpeed(motorSpeed);
  
  // set motor directions and run motors
  if (rightIsTrue) 
  {
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
  }
  else 
  {
    rightMotor->run(BACKWARD);
    leftMotor->run(FORWARD);
  }
  delay(1000);

  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
}

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Right turn test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
}

void loop() {
  // nothing in loop
  turn(100, true);
  delay(1000);
  turn(100, false);
  delay(1000);
}
