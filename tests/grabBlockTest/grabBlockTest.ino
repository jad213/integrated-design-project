#include <Servo.h>
/*#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Global variables
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor* rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor* leftMotor = AFMS.getMotor(2);*/
// create servo object
Servo leftServo;
Servo rightServo;
#define lnDistFromAxle 200// distance of line sensors from axle in mm
#define wheelWidth 220 // width in mm
#define leftServoPin 9
#define rightServoPin 10

int theta;
double motorSpeedCoeff = 1.43;


void sweepArm(Servo servo,double finalAngle){
  //servo.write(angle);
  int currentAngle = servo.read();
  if (finalAngle > currentAngle){
    for (theta = currentAngle; theta <= finalAngle; theta += 1) {
      servo.write(theta);
      delay(15);
      }
  }
  else if (finalAngle<currentAngle){
    for (theta = currentAngle; theta >= finalAngle; theta -= 1) {
      servo.write(theta);
      delay(15);
      }
  }
}
void fullyOpen(){
  //both arms closed
  sweepArm(leftServo,90);
  sweepArm(rightServo,90);
  for (theta = 0; theta<= 30;theta+=1){
    leftServo.write(90-theta);
    rightServo.write(90+theta);
    delay(15);
  }
}
void grabBlock(){
  //both arms fully open
  sweepArm(leftServo,60);
  sweepArm(rightServo,120);
  for (theta = 0; theta <= 35;theta+=1){
    leftServo.write(60+theta);
    rightServo.write(120-theta);
    delay(15);
  }
}

void setup() {
  // put your setup code here, to run once:
  leftServo.attach(leftServoPin);
  rightServo.attach(rightServoPin);
  //rightServo.write(0);
  Serial.begin(9600);
  leftServo.write(90);
  rightServo.write(90);
  //AFMS.begin();
}
void loop() {
  // put your main code here, to run repeatedly:
  /*sweepArm(rightServo,180);
  Serial.println(rightServo.read());
  delay(1000);
  sweepArm(rightServo,0);
  Serial.println(rightServo.read());
  delay(1000);
  fullyOpen();
  delay(3000);
  grabBlock();
  delay(3000);*/
  int leftAngle = leftServo.read();
  int rightAngle = rightServo.read();
  Serial.println("Left,Right:");
  Serial.println(leftAngle);
  Serial.println(rightAngle);
  delay(10000);
  fullyOpen();
  delay(10000);
  grabBlock();
  
  
}
