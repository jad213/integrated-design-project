int sensorPin = A1;
int sensorValue = 0;  // variable to store the value coming from the sensor
#define LED_PIN 1

void readLDR(int delayTime)
{
  delay((int) delayTime / 2);
  sensorValue = analogRead(sensorPin);  // get sensor value
  Serial.println(sensorValue); // print sensor value to serial
  delay((int) delayTime / 2);
}

void setup() {
  Serial.begin(9600); // set up serial libary at 9600 bps
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_PIN, HIGH);
  /*
  digitalWrite(LEDpin, HIGH);
  for (int i = 0; i < 5; i++)
  {
  readLDR(100);
  }

  digitalWrite(LEDpin, LOW);
  for (int i = 0; i < 5; i++)
  {
  readLDR(100);
  }*/
  readLDR(100);
}
