int sensorPin = A0;
int sensorValue = 0;  // variable to store the value coming from the sensor

void readAnalog(int delayTime)
{
  delay((int) delayTime / 2);
  sensorValue = analogRead(sensorPin);  // get sensor value
  Serial.println(sensorValue); // print sensor value to serial
  delay((int) delayTime / 2);
}

void setup() {
  Serial.begin(9600); // set up serial libary at 9600 bps
}

void loop() 
{
  readAnalog(10);
}
