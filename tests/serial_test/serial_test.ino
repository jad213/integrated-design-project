void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);           //Start serial and set the correct Baud Rate
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("Hello World");         //Sends Serial String with no end of line characters
  delay(1000);
}
