#include <Servo.h>

#define leftServoPin 8
#define rightServoPin 9
// create servo object
Servo leftServo;
Servo rightServo;
int theta;
void setup() {
  // put your setup code here, to run once:
  leftServo.attach(leftServoPin);
  rightServo.attach(rightServoPin);
  //rightServo.write(0);
  Serial.begin(9600);
}

void sweepArm(Servo servo,double finalAngle){
  //servo.write(angle);
  int currentAngle = servo.read();
  if (finalAngle > currentAngle){
    for (theta = currentAngle; theta <= finalAngle; theta += 1) {
      servo.write(theta);
      delay(15);
      }
  }
  else if (finalAngle<currentAngle){
    for (theta = currentAngle; theta >= finalAngle; theta -= 1) {
      servo.write(theta);
      delay(15);
      }
  }
}
void fullyOpen(){
  //both arms closed
  sweepArm(leftServo,90);
  sweepArm(rightServo,90);
  for (theta = 90; theta>= 0;theta-=1){
    leftServo.write(theta);
    rightServo.write(180-theta);
    delay(15);
  }
}
void grabBlock(){
  //both arms fully open
  sweepArm(leftServo,0);
  sweepArm(rightServo,180);
  for (theta = 0; theta <= 90;theta+=1){
    leftServo.write(theta);
    rightServo.write(180-theta);
    delay(15);
  }
}
void loop() {
  // put your main code here, to run repeatedly:
  sweepArm(rightServo,180);
  Serial.println(rightServo.read());
  delay(1000);
  sweepArm(rightServo,0);
  Serial.println(rightServo.read());
  delay(1000);
  
}
