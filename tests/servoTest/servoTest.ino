#include <Servo.h>

#define servo1Pin 9

// create servo object
Servo servo1;

void setup() {
  // put your setup code here, to run once:
  servo1.attach(servo1Pin);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  servo1.write(50);
  Serial.println(servo1.read());
  delay(2000);
  servo1.write(90);
  Serial.println(servo1.read());
  delay(2000);
}
