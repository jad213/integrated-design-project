#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Global variables
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor* rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor* leftMotor = AFMS.getMotor(2);
// create servo object
Servo leftServo;
Servo rightServo;
#define lnDistFromAxle 200// distance of line sensors from axle in mm
#define wheelWidth 220 // width in mm
#define leftServoPin 9
#define rightServoPin 10
#define LED_PIN_RED 1
#define LED_PIN_BLUE 2
#define wheelWidth 220 // width in mm
#define servoMaxOpenAngle = 45;//degrees
int theta;
double motorSpeedCoeff = 1.43;
int sensorValue = 0;
int sensorPin = A1;
enum blockColour {RED = 0, BLUE = 1, NONE = 2}

int ambientBlue = 0;
int ambientRed = 0;//for calibration
void readLDR(int delayTime)
{
  delay((int) delayTime / 2);
  sensorValue = analogRead(sensorPin);  // get sensor value
  //Serial.println(sensorValue); // print sensor value to serial
  delay((int) delayTime / 2);
}

void sweepArm(Servo servo,double finalAngle){
  //servo.write(angle);
  int currentAngle = servo.read();
  if (finalAngle > currentAngle){
    for (theta = currentAngle; theta <= finalAngle; theta += 1) {
      servo.write(theta);
      delay(15);
      }
  }
  else if (finalAngle<currentAngle){
    for (theta = currentAngle; theta >= finalAngle; theta -= 1) {
      servo.write(theta);
      delay(15);
      }
  }
}
void openArms(int angle){
  //angle from 90 degree vertical outwards
  //both arms closed
  sweepArm(leftServo,90);
  sweepArm(rightServo,90);
  for (theta = 0; theta<= angle;theta+=1){
    leftServo.write(90-theta);
    rightServo.write(90+theta);
    delay(15);
  }
}
void closeArms(int angle){
  //angle closed (measured from outside horizontal)
  //both arms fully open
  openArms(servoMaxOpenAngle);
  for (theta = servoMaxOpenAngle; theta <= angle;theta+=1){
    leftServo.write(theta);
    rightServo.write(180-theta);
    delay(15);
  }
}

void setVelocity(double velocity, double angularVelocity)
// velocity in mm/s, angularVelocity about vertical axis in rad/s
// sets motors to move the robot at ~velocity and angularVelocity
{
  double rightWheelVel = velocity + angularVelocity * wheelWidth * 0.5; // right wheel velocity in mm/s
  double leftWheelVel = velocity - angularVelocity * wheelWidth * 0.5; // left wheel velocity in mm/s

  rightMotor->setSpeed(min(abs(rightWheelVel * motorSpeedCoeff), 255));
  leftMotor->setSpeed(min(abs(leftWheelVel * motorSpeedCoeff), 255));

  rightMotor->run(rightWheelVel > 0 ? FORWARD : BACKWARD);
  leftMotor->run(leftWheelVel > 0 ? FORWARD : BACKWARD);
}

void calibrateColourSensor() {
  digitalWrite(LED_PIN_RED,LOW);
  digitalWrite(LED_PIN_BLUE,HIGH);
  delay(1000);
  readLDR(100);
  ambientBlue = sensorValue;
  digitalWrite(LED_PIN_RED,LOW);
  digitalWrite(LED_PIN_BLUE,LOW);
  delay(1000);
  digitalWrite(LED_PIN_RED,HIGH);
  digitalWrite(LED_PIN_BLUE, LOW);
  readLDR(100);
  ambientRed = sensorValue;
}
blockColour getBlockColour() {
  digitalWrite(LED_PIN_BLUE,LOW);
  digitalWrite(LED_PIN_RED,HIGH);
  readLDR(100);
  int onRed = sensorValue;
  digitalWrite(LED_PIN_RED,LOW);
  digitalWrite(LED_PIN_BLUE,HIGH);
  readLDR(100);
  int onBlue = sensorValue;
  digitalWrite(LED_PIN_BLUE,LOW);
  int diffRed = onRed-ambientRed;
  int diffBlue = onBlue-ambientBlue;
  int diffBlueRed = onBlue - onRed;
  if (diffBlueRed < -20){
    return BLUE;
  }
  else if (diffBlueRed > 15){
    return RED;
  }
  else {
    return NONE;
  }
}//needs testing diffbluered or diff from ambient
void printSensorValues() {
    digitalWrite(LED_PIN_BLUE, LOW);
    digitalWrite(LED_PIN_RED, HIGH);
    readLDR(100);
    int onRed = sensorValue;
    digitalWrite(LED_PIN_RED, LOW);
    digitalWrite(LED_PIN_BLUE, HIGH);
    readLDR(100);
    int onBlue = sensorValue;
    digitalWrite(LED_PIN_BLUE, LOW);
    int diffRed = onRed - ambientRed;
    int diffBlue = onBlue - ambientBlue;
    Serial.println("Ambient");
    Serial.println(ambientRed);
    Serial.println("Diff Red");
    Serial.println(diffRed);
    Serial.println("Diff Blue");
    Serial.println(diffBlue);
    Serial.println("Diff blured");
    Serial.println(onBlue-onRed);
}
int returnAmbientDifference(){
  digitalWrite(LED_PIN_BLUE, LOW);
  digitalWrite(LED_PIN_RED, HIGH);
  readLDR(100);
  int onRed = sensorValue;
  digitalWrite(LED_PIN_RED, LOW);
  digitalWrite(LED_PIN_BLUE, HIGH);
  readLDR(100);
  int onBlue = sensorValue;
  digitalWrite(LED_PIN_BLUE, LOW);
  int diffRed = onRed - ambientRed;
  int diffBlue = onBlue - ambientBlue;
  //return onBlue-onRed;
  return diffBlue - diffRed;
}//needs testing
void repositionIfDetectBlock(){
  currentColour = getBlockColour();
  
  if ((currentColour == BLUE) or (currentColour == RED)){
    openArms(10);
    setVelocity(100,0);//replace with lineFollowing for small time
    delay(2000);//block in front
    closeArms(95);
  }
  else {
    openArms(servoMaxOpenAngle);
  }
}
void setup() {
  // put your setup code here, to run once:
  leftServo.attach(leftServoPin);
  rightServo.attach(rightServoPin);
  Serial.begin(9600);
  pinMode(LED_PIN_RED, OUTPUT);
  pinMode(LED_PIN_BLUE, OUTPUT);
  AFMS.begin();
  calibrateColorSensor();
}
void loop() {
  // put your main code here, to run repeatedly:
  /*int leftAngle = leftServo.read();
  int rightAngle = rightServo.read();
  Serial.println("Left,Right:");
  Serial.println(leftAngle);
  Serial.println(rightAngle);
  delay(5000);
  openArms(30);
  delay(5000);
  grabBlock(95);*/
  repositionIfDetectBlock();
  delay(2000);
  
}
