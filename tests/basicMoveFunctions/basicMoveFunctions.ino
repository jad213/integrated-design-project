// basic movement functions
#include <Wire.h>
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor *leftMotor = AFMS.getMotor(2);

enum turnDirection { LEFT = 0, RIGHT = 1 };

// movement functions
void turnRobot(int motorSpeed, int turnTime, turnDirection rotDirection)
{
  // set motor speeds
  rightMotor->setSpeed(motorSpeed);
  leftMotor->setSpeed(motorSpeed);

  // set motor directions and run motors
  if (rotDirection)  
  {
    rightMotor->run(BACKWARD);
    leftMotor->run(FORWARD);
  }
  else
  {
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
  }
  delay(turnTime);

  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
}

void moveInStraightLine(int motorSpeed, int moveTime, bool moveDirection)
{
  // set motor speeds
  rightMotor->setSpeed(motorSpeed);
  leftMotor->setSpeed(motorSpeed);
 

  if (moveDirection) 
  {
    // set diretion 
    rightMotor->run(FORWARD);
    leftMotor->run(FORWARD);
  }
  else 
  {
    // set diretion 
    rightMotor->run(BACKWARD);
    leftMotor->run(BACKWARD);
  }

  // wit delay time with motors running
  delay(moveTime);
  
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
}

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor movement functions!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
}

void loop() {
  // put your main code here, to run repeatedly:
  moveInStraightLine(150, 3000, 1);
  //delay(1000);

  //moveInStraightLine(200, 3000, 1);
  //delay(1000);
  //turnRobot(100, 3141, RIGHT);
  //delay(1000);
  //moveInStraightLine(100, 5000, 0);
  //delay(1000);
}
