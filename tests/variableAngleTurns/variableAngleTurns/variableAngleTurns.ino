
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#define lnDistFromAxle 200// distance of line sensors from axle in mm
#define wheelWidth 220 // width in mm

// Global variables
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor* rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor* leftMotor = AFMS.getMotor(2);
double motorSpeedCoeff = 1.43;
double turnSpeed = 1.0 ;//rad/sec
double turnTime;
double pi = 3.14159;
void setVelocity(double velocity, double angularVelocity)
// velocity in mm/s, angularVelocity about vertical axis in rad/s
// sets motors to move the robot at ~velocity and angularVelocity
{
  double rightWheelVel = velocity + angularVelocity * wheelWidth * 0.5; // right wheel velocity in mm/s
  double leftWheelVel = velocity - angularVelocity * wheelWidth * 0.5; // left wheel velocity in mm/s

  rightMotor->setSpeed(min(abs(rightWheelVel * motorSpeedCoeff), 255));
  leftMotor->setSpeed(min(abs(leftWheelVel * motorSpeedCoeff), 255));

  rightMotor->run(rightWheelVel > 0 ? BACKWARD : FORWARD);
  leftMotor->run(leftWheelVel > 0 ? BACKWARD : FORWARD);
  /*Serial.println("Moving");
  Serial.println(rightWheelVel);
  Serial.println(leftWheelVel);*/
}
void turnRobot(double turnAngle)
//turnAngle in rad
{
  turnTime = turnAngle/turnSpeed;
  setVelocity(0,turnSpeed);
  /*Serial.println("Turning");
  Serial.println(turnSpeed);
  Serial.println(turnAngle);
  Serial.println(turnTime);*/
  //delay(turnTime*1000);
  //rightMotor->run(RELEASE);
  //leftMotor->run(RELEASE);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  AFMS.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  setVelocity(100,0);
  delay(2000*turnTime);
  Serial.println("");
  double angle = 90.0;
  double turnAngle = angle*3.14159/180.0;//radians
  //Serial.println("Angle");
  //Serial.println(angle);
  turnRobot(turnAngle);
  delay(1000*turnTime);
}
