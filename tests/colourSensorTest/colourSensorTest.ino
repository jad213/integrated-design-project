
enum blockColour {RED = 0, BLUE =1, NONE = 2};

int sensorPin = A1;
int sensorValue = 0;  // variable to store the value coming from the sensor
#define LED_PIN_RED 1
#define LED_PIN_BLUE 2

void readLDR(int delayTime)
{
  delay((int) delayTime / 2);
  sensorValue = analogRead(sensorPin);  // get sensor value
  //Serial.println(sensorValue); // print sensor value to serial
  delay((int) delayTime / 2);
}

void setup() {
  Serial.begin(9600); // set up serial libary at 9600 bps
  pinMode(LED_PIN_RED, OUTPUT);
  pinMode(LED_PIN_BLUE, OUTPUT);
}

blockColour getBlockColour() {
  digitalWrite(LED_PIN_RED,LOW);
  digitalWrite(LED_PIN_BLUE,LOW);
  readLDR(100);
  int ambientRed = sensorValue;
  int ambientBlue = sensorValue;
  digitalWrite(LED_PIN_RED,HIGH);
  readLDR(100);
  int onRed = sensorValue;
  digitalWrite(LED_PIN_RED,LOW);
  digitalWrite(LED_PIN_BLUE,HIGH);
  readLDR(100);
  int onBlue = sensorValue;
  digitalWrite(LED_PIN_BLUE,LOW);
  int diffRed = onRed-ambientRed;
  int diffBlue = onBlue-ambientBlue;
  int diffBlueRed = onBlue - onRed;
  if (diffBlueRed < -20){
    return BLUE;
  }
  else if (diffBlueRed > 15){
    return RED;
  }
  else {
    return NONE;
  }
}
void printSensorValues() {
    digitalWrite(LED_PIN_RED, LOW);
    digitalWrite(LED_PIN_BLUE, LOW);
    readLDR(100);
    int ambientRed = sensorValue;
    int ambientBlue = sensorValue;
    digitalWrite(LED_PIN_RED, HIGH);
    readLDR(100);
    int onRed = sensorValue;
    digitalWrite(LED_PIN_RED, LOW);
    digitalWrite(LED_PIN_BLUE, HIGH);
    readLDR(100);
    int onBlue = sensorValue;
    digitalWrite(LED_PIN_BLUE, LOW);
    int diffRed = onRed - ambientRed;
    int diffBlue = onBlue - ambientBlue;
    Serial.println("Ambient");
    Serial.println(ambientRed);
    Serial.println("Diff Red");
    Serial.println(diffRed);
    Serial.println("Diff Blue");
    Serial.println(diffBlue);
    Serial.println("Diff blured");
    Serial.println(onBlue-onRed);
}
void loop() {
  printSensorValues();
  delay(3000);
  //digitalWrite
  /*
  digitalWrite(LEDpin, HIGH);
  for (int i = 0; i < 5; i++)
  {
  readLDR(100);
  }

  digitalWrite(LEDpin, LOW);
  for (int i = 0; i < 5; i++)
  {
  readLDR(100);
  }*/
  
}
