// file for testing line following

// includes
#include <Wire.h>
#include <Adafruit_MotorShield.h>

// set pin mubers for left, middle, right and bottom line sensors (plan view)
#define lnLeftPin 0
#define lnMidPin 1
#define lnRightPin 2
#define lnBackPin 3
// set physical parameters
#define lnSpacing 20 // spacing between line sensors in mm
#define lnDistFromAxle 160 // distance of line sensors from axle in mm
#define wheelWidth 220 // width in mm

// Global variables
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor* rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor* leftMotor = AFMS.getMotor(2);

// ~linear fit between motor speed and wheel velocity
double motorSpeedCoeff = 1.43;

// functions

void setVelocity(double velocity, double angularVelocity)
// velocity in mm/s, angularVelocity about vertical axis in rad/s
// sets motors to move the robot at ~velocity and angularVelocity
{
  double rightWheelVel = velocity + angularVelocity * wheelWidth * 0.5; // right wheel velocity in mm/s
  double leftWheelVel = velocity - angularVelocity * wheelWidth * 0.5; // left wheel velocity in mm/s

  rightMotor->setSpeed(min(abs(rightWheelVel * motorSpeedCoeff), 255));
  leftMotor->setSpeed(min(abs(leftWheelVel * motorSpeedCoeff), 255));

  rightMotor->run(rightWheelVel > 0 ? FORWARD : BACKWARD);
  leftMotor->run(leftWheelVel > 0 ? FORWARD : BACKWARD);
}

void followLine(double maxVel, double angularGain, double angularDerivativeGain, double slowFactor, double rightBias)
// maxVel in mm/s, dampingCoeff is for the second order linear system that is the line following control,
// slowFactor controls how much the robot slows down to move round corners,
// rightBias controlls the bias of the robot to turn right, set negative to bias left, this is used to turn the correct way at junctions
{
  // declare variables
  bool lnLeft, lnMid, lnRight, lnBack;
  double lineOffset, lineGain, velDesired, angularVelDesired, angularVelDerivative;
  int frontLineSensorSum;

  static double angularVelDesiredLastCall = 0;

  // read light sensors
  lnLeft = digitalRead(lnLeftPin);
  lnMid = digitalRead(lnMidPin);
  lnRight = digitalRead(lnRightPin);
  //lnBack = digitalRead(LN_BACK);

  // find sum of line sensor outputs
  frontLineSensorSum = lnLeft + lnMid + lnRight;

  if (frontLineSensorSum == 0)
    // if all the sensors are all off, do not change the current motor speeds, just carry on at current speed, i.e return
  {
    return;
  }
  else
  {
    // find the aproximate line offset as an average of the line sensors, use max() to avoid div 0 when not on line
    // the left and right line sensors are weighted by rightBias in order to make the robot preferentially turn into the right direction for rightBias > 0,

    lineOffset = lnSpacing * ((1 - rightBias) * lnLeft - (1 + rightBias) * lnRight) / max(frontLineSensorSum, 1);

    velDesired = maxVel * (1 - slowFactor * abs(lineOffset) / lnSpacing);

    angularVelDerivative = (angularVelDesired - angularVelDesiredLastCall) / 0.01;

    angularVelDesired = angularGain * lineOffset;
    angularVelDesired = angularVelDesired + angularDerivativeGain / lnDistFromAxle * angularVelDerivative; // rad/s

    // set motor velocity
    setVelocity(velDesired, angularVelDesired);
  }

  angularVelDesiredLastCall = angularVelDesired;
}

void printLineSensorReadings()
// prints the line sensor readings to the serial
{
  // print line sensor readings to Serial
  Serial.print("L: "); Serial.println(digitalRead(lnLeftPin));
  Serial.print("M: "); Serial.println(digitalRead(lnMidPin));
  Serial.print("R: "); Serial.println(digitalRead(lnRightPin));
  Serial.print("B: "); Serial.println(digitalRead(lnBackPin));
  Serial.println("    ");
  delay(100);
}

// setup and loop

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  AFMS.begin();

  // setup pins (not strictly nessecsary
  pinMode(lnLeftPin, INPUT);
  pinMode(lnMidPin, INPUT);
  pinMode(lnRightPin, INPUT);
  pinMode(lnBackPin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  //followLine(200, 0.05, 0,  0.8, 0.0);
  delay(100);
  printLineSensorReadings();
}
