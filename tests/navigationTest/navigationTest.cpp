#include <iostream>

#define numNodes 8

using namespace std;

// class to handle arrays of size NUMNODE more easily, shame we can't use std::vector
class numNodesIntArray
{
public:
    int nodes[numNodes];
    numNodesIntArray() { for (int i = 0; i < numNodes; i++) nodes[i] = -1; }
    numNodesIntArray(int initialValue) { for (int i = 0; i < numNodes; i++) nodes[i] = initialValue; }
    bool operator== (const numNodesIntArray comp) const {
        for (int i = 0; i < numNodes; i++) { if (comp.nodes[i] != nodes[i]) return false; }
        return true;
    }
};

numNodesIntArray findNodePath(int startNode, int endNode)
// finds the shortest node path between two nodes using a simplified version of Djikstra's algorithm
// returns a numNodesIntArray with nodes = {startNode, ..., endNode, -1, -1, .. -1}
// -1 is used as a null value for no node
{
    // array of shortest paths known to each node currently, use -1 for unused space
    numNodesIntArray shortestFoundPath[numNodes];
    numNodesIntArray dist, visited(0);
    int i, currentNode, node, depth = 0, minNode, minNodeDist;

    // set the first node to startNode
    for (node = 0; node < numNodes; node++) shortestFoundPath[node].nodes[0] = startNode;
    // set start node dist to 0
    dist.nodes[startNode] = 0;

    for (depth = 1; depth < numNodes; depth++)
    {
        // find closest node not visited
        minNode = -1;
        minNodeDist = numNodes;
        for (node = 0; node < numNodes; node++)
        {
            if (!visited.nodes[node])
            {
                if ((dist.nodes[node] != -1) && (dist.nodes[node] < minNodeDist))
                {
                    minNode = node;
                    minNodeDist = dist.nodes[node];
                }
            }
        }

        // set current node as the minNode found before
        currentNode = minNode;

        // mark node as visited
        visited.nodes[currentNode] = 1;

        // iterate over all nodes to check if they are connected to minNode
        for (node = 0; node < numNodes; node++)
        {
            // check if theres a path between nodes
            if (nodeTable(node, currentNode))
            {
                // check if node has not been visited before
                if (dist.nodes[node] == -1)
                {
                    // set the shortest path as the path through currentNode
                    // this will always be the shortest path as the nodes are all unit length from each other in this model
                    shortestFoundPath[node] = shortestFoundPath[currentNode];

                    // set next node in shortestFoundPath[node] to node
                    // use while loop to find the next node in shortestFoundPath
                    i = 0;
                    while ((shortestFoundPath[node].nodes[i] != -1) && (i < numNodes)) i++;
   
                    shortestFoundPath[node].nodes[i] = node;

                    // set the distance as depth and set as visited
                    dist.nodes[node] = depth;

                    // return the shortest path if the endNode has been reached
                    if (node == endNode)
                    {
                        return shortestFoundPath[node];
                    }
                }
            }
        }
    }

    // if function fails, return array of -1
    return numNodesIntArray(-1);
}

int main()
{
    cout << "Main" << endl;
    numNodesIntArray nodePath;
    nodePath = findNodePath(4, 6);
    for (int i = 0; i < numNodes; i++) 
    {
        cout << nodePath.nodes[i] << " ";
    }
};