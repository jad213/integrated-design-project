#define LED_PIN 13

void setup() {
  // put your setup code here, to run once:
pinMode(LED_PIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED_PIN, HIGH);       // sets the digital pin 13 on
  delay(1000);                  // waits for a second
  digitalWrite(LED_PIN, LOW);        // sets the digital pin 13 off
  delay(1000);                  // waits for a second
}
