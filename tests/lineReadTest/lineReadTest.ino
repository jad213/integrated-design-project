#define LINEPIN 0
#define ANALOGPIN A0

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Digital lineread pin");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print(digitalRead(LINEPIN));
  Serial.print("    ");
  //Serial.println(analogRead(ANALOGPIN));
  delay(100);
}
