// code to run ALBIE robot

// includes
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>

// set pin numbers
// left, middle, right and bottom line sensors (plan view)
#define lnLeftPin 3
#define lnMidPin 1
#define lnRightPin 2
#define lnBackPin 0
// colour sensor pins
#define colourSenseRedLed 4
#define colourSenseBlueLed 5
#define colourSensorAnalogPin A1
// servo pins
#define leftServoPin 9
#define rightServoPin 10
// LED pins
#define blockMovingAmberLedPin 11
#define blockMovingRedLedPin 12
#define blockMovingBlueLedPin 13
// switch pin
#define switchPin 7

// set physical parameters
#define lnSpacing 23 // spacing between line sensors in mm
#define lnDistFromAxle 165 // distance of line sensors from axle in mm
#define wheelWidth 220.0 // width in mm
#define servoMaxOpenAngle 60 // degrees, measured w.r.t. robot longitudinal axis

// line following parameters
#define lnFollowVelocity 150 // mm/s
#define lnJunctionVelocity 100 // mm/s
#define lnFollowGain 0.02 // Gain between angular velocity and line displacement
#define lnFollowGainJunction 0.05 // higher gain for handeling junctions
#define lnSlowCoeff 0.5 // Controls how much ALBIE slows down for corners
// navigation parameters
#define numNodes 8 // Number of nodes



// enums and classes for code clarity
enum blockCarryingStatus { NO_BLOCK = 0, RED = 1, BLUE = 2 };

// for leftRight, left and right are always from the point of view of a plan view, with forward facing being vertical
enum leftRight { LEFT = 1, RIGHT = 2 };

// the block positions given by which nodes they are between. NODE24_1 is the closer block to node 2 and NODE24_2 the closer block to node 4
enum blockLinePosition { NODE23 = 0, NODE34 = 1, NODE24_1 = 2, NODE24_2 = 3, NODE01 = 4, ALLBLOCKSMOVED = -1 };

// INITIAL - block is in intial position
// TARGET - block has been put in target area
// STORED - block has been stored between nodes 0 & 1
// LOST - block position is unknown
enum blockStatus { INITIAL = 0, TARGET = 1, STORED = 2, CARRYING = 3, LOST = 4 };

// class to store which two nodes ALBIE is inbetween, and in what order
class positionWrtNode
{
public:
    positionWrtNode() { lastNode = 0; nextNode = 0; }
    positionWrtNode(int lastNode, int nextNode) { positionWrtNode::lastNode = lastNode; positionWrtNode::nextNode = nextNode; }
    // changes nodes when ALBIE moves across a node
    void moveNode(int nextNode) { positionWrtNode::lastNode = positionWrtNode::nextNode; positionWrtNode::nextNode = nextNode; }
    unsigned int lastNode, nextNode;
};

// class to handle arrays of size NUMNODE more easily, shame we can't use std::vector
class numNodesIntArray
{
public:
    int nodes[numNodes];
    numNodesIntArray() { for (int i = 0; i < numNodes; i++) nodes[i] = -1; }
    numNodesIntArray(int initialValue) { for (int i = 0; i < numNodes; i++) nodes[i] = initialValue; }
    bool operator== (const numNodesIntArray comp) const {
        for (int i = 0; i < numNodes; i++) { if (comp.nodes[i] != nodes[i]) return false; }
        return true;
    }
};



// Global variables
// create the motor shield object
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// create motor objects
Adafruit_DCMotor* rightMotor = AFMS.getMotor(1);
Adafruit_DCMotor* leftMotor = AFMS.getMotor(2);
// create servo object
Servo leftServo;
Servo rightServo;
  
// variables for current state of robot
positionWrtNode posWrtNode(-1, 0);
bool switchOn = 0;

// line senor readings
bool lnLeft, lnMid, lnRight, lnBack;

// colour sensor ambient levels
int ambientBlue, ambientRed;

// ~linear fit between motor speeds and wheel velocities
double rightMotorSpeedCoeff = 1.818;
double leftMotorSpeedCoeff = 1.695;

// keeps track of if the blocks have been moved or not
blockStatus blockPositions[4] = { INITIAL };
// keeps track of whether a block has been placed at a placement node or not
// index:
// 0 - red 1 (node 3)
// 1 - red 2 (node 4)
// 2 - blue 1 (node 5)
// 3 - blue 2 (node 6)
bool blockPlacedAtNode[4] = { 0 }; 



// sensor functions
void readLineSensors()
// updates line sensor global variables by reading pins
{
    lnLeft = digitalRead(lnLeftPin);
    lnMid = digitalRead(lnMidPin);
    lnRight = digitalRead(lnRightPin);
    lnBack = digitalRead(lnBackPin);
}

int readLDR(int delayTime)
// delayTime in millisecs
{
    int out;
    delay(0.5 * delayTime);
    out = analogRead(colourSensorAnalogPin);
    delay(0.5 * delayTime);
    return out;
}

blockCarryingStatus readBlockColour() 
{
    int ambientAll, diffBlue, diffRed;

    updateColourSensorReadings(ambientAll,diffRed,diffBlue);
    //Very strong values of difference from ambient for red and blue blocks, once we taped the sides of the colour sensor
    //to stop light getting in,so if diffBlue and diffRed are in these respective ranges, then we can definitively determine
    //the colour of the block (as the surface reflecting light towards the LDR is the same regardless of ambient variations if
    //there is a block directly in front of the robot. However, if diffBlue and diffRed are outside these ranges, we cannot determine the 
    //presence of the block, or its color.
    if ((diffBlue> -200) &&(diffBlue<-170)&&(diffRed>-360)&&(diffRed<-330)){
        Serial.println("BLOCK IS BLUE");

        digitalWrite(colourSenseRedLed, LOW);
        digitalWrite(colourSenseBlueLed, HIGH);

        return BLUE;
    }
    else if ((diffBlue > -135) && (diffBlue < -105) && (diffRed > -520) && (diffRed < -420)) {
        Serial.println("BLOCK IS RED");

        digitalWrite(colourSenseRedLed, HIGH);
        digitalWrite(colourSenseBlueLed, LOW);

        return RED;
    }
    else {
        Serial.println("COLOUR UNCERTAIN OR NO BLOCK");
        return NO_BLOCK;

        digitalWrite(colourSenseRedLed, LOW);
        digitalWrite(colourSenseBlueLed, LOW);
    }
}

void printColourSensorValues()
{
    digitalWrite(colourSenseBlueLed, LOW);
    digitalWrite(colourSenseRedLed, LOW);
    delay(1000);

    int ambientAll = readLDR(100);

    digitalWrite(colourSenseRedLed, LOW);
    digitalWrite(colourSenseBlueLed, HIGH);
    delay(1000);

    int onBlue = readLDR(100);
    digitalWrite(colourSenseRedLed, HIGH);
    digitalWrite(colourSenseBlueLed, LOW);
    delay(1000);

    int onRed = readLDR(100);
    digitalWrite(colourSenseBlueLed, LOW);


    int diffRed = onRed - ambientAll;
    int diffBlue = onBlue - ambientAll;

    Serial.println("Ambient ");
    Serial.println(ambientAll);
    Serial.println("Diff Blue");
    Serial.println(diffBlue);
    Serial.println("Diff Red");
    Serial.println(diffRed);
    Serial.println("onBlue - onRed");
    Serial.println(onBlue - onRed);
    Serial.println("diffBlue - diffRed");
    Serial.println(diffBlue - diffRed);
    Serial.println(" ");
    Serial.println(" ");
    if (abs(diffBlue) + abs(diffRed) > 300) {
        Serial.println("BLOCK DETECTED");
        if ((diffBlue - diffRed > 100) && (diffBlue - diffRed < 180) ){
            Serial.println("BLOCK BLUE");
        }
        else if ((diffBlue - diffRed > 180) && (diffBlue - diffRed < 280)) {
            Serial.println("BLOCK RED");
        }
    }
    else {
        Serial.println("NO BLOCK");
    }
}

void shuffleForwards() {
    //The robot performs a small shuffle forwards along the line, to nudge the block so that it 
    //is consistently directly in front of the colour sensor, blocking any other ambient light to
    //the LDR
    for (int i = 0; i < 3; i++) {//can increase max value of i for more shuffles if increased accuracy is required
        openArms(10);
        
        followLineForDistance(50, 0, 30);
        setVelocity(50, 0);
        closeArms(90);
        delay(50);
        releaseMotors();
    }
}

void updateColourSensorReadings(int& ambientAll, int& diffRed, int& diffBlue) 
{
    int ledDelay = 1000;//add a delay after turning LEDs on/off before reading values to allow stabilisation time

    digitalWrite(colourSenseBlueLed, LOW);
    digitalWrite(colourSenseRedLed, LOW);
    delay(ledDelay);
    ambientAll = readLDR(100);//first take ambient light value of LDR with both LEDs off
   

    digitalWrite(colourSenseRedLed, LOW);
    digitalWrite(colourSenseBlueLed, HIGH);
    delay(ledDelay);
    int onBlue = readLDR(100);//measure value with blue LED
    
    digitalWrite(colourSenseRedLed, HIGH);
    digitalWrite(colourSenseBlueLed, LOW);
    delay(ledDelay);
    int onRed = readLDR(100);//and with red

    digitalWrite(colourSenseBlueLed, LOW);
    digitalWrite(colourSenseRedLed, LOW);//make sure LEDs turned off afterwards to conserve power

    diffRed = onRed - ambientAll;//calculate the difference, very reliably distinguish between red/blue blocks
    diffBlue = onBlue - ambientAll;//if the block is pushed right up to the colour sensor
}

void openArms(int angle) {
    //angle measured from 90 degree vertical outwards
    //to make servo movement less jerky and avoid accidentally knocking blocks by moving too quickly
    for (int theta = 0; theta <= angle; theta += 1) //gradually varies theta and sweeps from closed to the required open angle (twice the input angle)
    {
        leftServo.write(90 - theta);
        rightServo.write(90 + theta);
        delay(15);
    }
}

void closeArms(int angle) 
{
    //angle closed (measured from outside horizontal)
    //to make servo movement less jerky and avoid accidentally knocking blocks by moving too quickly
    //closeArms(75) is the same as openArms(15)
    for (int theta = 45; theta <= angle; theta += 1) //gradually sweeps both arms in from 45 degrees (sets to 45 if not already)
    {
        leftServo.write(theta);
        rightServo.write(180 - theta);
        delay(15);
    }
}



// LED & switch functions
void updateAmberLed()
{
    digitalWrite(blockMovingAmberLedPin, (millis() / 500) % 2);
}

void delayWhileUpdatingAmberLed(int time)
// delay for time (milliseconds) while still flashing amber led
{
    for (int elapsedTime = 0; elapsedTime < time; elapsedTime += 20)
    {
        updateAmberLed();
        delay(20);
    }
}

void switchInterupt()
{
    // change switch on state
    switchOn = 1;
}



// movement functions
void releaseMotors()
{    
    // release motors
    rightMotor->run(RELEASE);
    leftMotor->run(RELEASE);

    // turn off amber led
    digitalWrite(blockMovingAmberLedPin, 0);
}

void setVelocity(double velocity, double angularVelocity)
// velocity in mm/s, angularVelocity about vertical axis in rad/s
// sets motors to move the robot at ~velocity and angularVelocity
// also flashes the amber LED when moving
{
    double rightWheelVel = velocity + angularVelocity * wheelWidth * 0.5; // right wheel velocity in mm/s
    double leftWheelVel = velocity - angularVelocity * wheelWidth * 0.5; // left wheel velocity in mm/s

    // set motor speeds
    rightMotor->setSpeed(min(abs(rightWheelVel * rightMotorSpeedCoeff), 255));
    leftMotor->setSpeed(min(abs(leftWheelVel * leftMotorSpeedCoeff), 255));

    // run motors
    rightMotor->run(rightWheelVel > 0 ? FORWARD : BACKWARD);
    leftMotor->run(leftWheelVel > 0 ? FORWARD : BACKWARD);

    // flash amber LED
    updateAmberLed();
}

void moveDistance(int distance, int speed)
// moves approximately distance mm forward
{
    double moveTime;
    // find time of movement
    moveTime = abs(distance) / double(speed);

    // set speed forward or backward
    if (distance > 0) setVelocity(speed, 0);
    else setVelocity(-speed, 0);

    // move forward for moveTime
    delayWhileUpdatingAmberLed(int(moveTime * 1000));

    releaseMotors();
}

void turnAngle(double angle)
// angle in degrees, as a rotation around vertical axis, so anticlockwise is positive
{
    unsigned int turnTime; // turnTime in millisecs, 
    double turnSpeed; // turnSpeed in rad/s
    double turnWheelSpeed = 100; // wheel speed in mm/s
    
    // find turnSpeed from turnWheelSpeed
    turnSpeed = turnWheelSpeed * rightMotorSpeedCoeff / wheelWidth;
    // find turnTime from turnSpeed and angle
    turnTime = abs(1000 * angle * 3.141569 / 180 / turnSpeed);
    // set motor speed
    if (angle > 0)  setVelocity(0, turnSpeed);
    else setVelocity(0, -turnSpeed);
    // wait for turnTime
    delayWhileUpdatingAmberLed(turnTime);

    // stop motors
    releaseMotors();
}

void turnSmallAngle(double angle)
// angle in degrees, as a rotation around vertical axis, so anticlockwise is positive
// only turns one wheel, so finer resolution
{
    unsigned int turnTime; // turnTime in millisecs, 
    double turnSpeed; // turnSpeed in rad/s
    double turnWheelSpeed = 30; // wheel speed in mm/s

    // find turnSpeed from turnWheelSpeed
    turnSpeed = turnWheelSpeed * rightMotorSpeedCoeff / wheelWidth;
    // find turnTime from turnSpeed and angle
    turnTime = abs(1000 * angle * 3.141569 / 180 / turnSpeed);
    // set motor speed
    if (angle > 0)  setVelocity(turnWheelSpeed, turnSpeed);
    else setVelocity(turnWheelSpeed, -turnSpeed);
    // wait for turnTime
    delayWhileUpdatingAmberLed(turnTime);

    // stop motors
    releaseMotors();
}

void centerOnLine()
{
    unsigned int maxIt = 6, it;
    double angle = 0;
    // find left line
    it = 0;

    while ((lnMid) && (it < maxIt))
    {
        turnSmallAngle(-0.5);
        delay(100);
        readLineSensors();
        it++;
    }

    turnSmallAngle(5);
    angle += 4.75;
    readLineSensors();

    it = 0;
    // measure angle to right line
    while ((lnMid) && (it < 2 * maxIt))
    {
        turnSmallAngle(0.5);
        delay(100);
        angle += 0.5;
        readLineSensors();
        it++;
    }

    turnSmallAngle(-0.5 * angle);
}

void followLine(int speed, double rightBias, double gain)
// maxVel in mm/s, dampingCoeff is for the second order linear system that is the line following control, 
// slowFactor controls how much the robot slows down to move round corners,
// rightBias controlls the bias of the robot to turn right, set negative to bias left, this is used to turn the correct way at junctions
{
    // declare variables
    double lineOffset, velDesired, angularVelDesired;
    int frontLineSensorSum;

    // read line sensors
    readLineSensors();

    // find sum of line sensor outputs
    frontLineSensorSum = lnLeft + lnMid + lnRight;

    if (frontLineSensorSum == 0)
        // if all the sensors are all off, do not change the current motor speeds, just carry on at current speed, i.e return
    {
        return;
    }
    else
    {
        // find the aproximate line offset as an average of the line sensors, use max() to avoid div 0 when not on line
        // the left and right line sensors are weighted by rightBias in order to make the robot preferentially turn into the right direction for rightBias > 0,

        lineOffset = lnSpacing * ((1 - rightBias) * lnLeft - (1 + rightBias) * lnRight) / max(frontLineSensorSum, 1);

        velDesired = speed * (1 - lnSlowCoeff * abs(lineOffset) / lnSpacing);

        angularVelDesired = gain * lineOffset; // rad/s

        // set motor velocity
        setVelocity(velDesired, angularVelDesired);
    }
}

void followLineForTime(double speed, double rightBias, double lineFollowTime)
// follows the line for time lineFollowTime (seconds) at speed speed (mm/s)
{
    int timeElapsed = 0;

    setVelocity(lnFollowVelocity, 0);

    while (timeElapsed < lineFollowTime * 1000)
    {
        followLine(speed, rightBias, lnFollowGainJunction);
        delay(10);
        timeElapsed += 10;
    }
    
    releaseMotors();
}

void followLineForDistance(int speed, double rightBias, int distance)
// follows the line for distance mm, at speed mm/s, with rghtBias for line following
{
    int time;
    time = distance / speed;
    followLineForTime(speed, rightBias, time);
}

void turnOnLine(leftRight turnDirection, int maxTime)
// direction is directionthat the robot turns in, stops turning after maxTime (seconds) has elapsed if it can't find line again
{
    unsigned int elapsedTime = 0;
    int initialOffsetAngle = 40; // angle it moves off line before turning and looking for line
    double angularVel = 100 / (0.5 * wheelWidth); // angular vel when turning in rad/s

    // turn through initialOffsetAngle to get off the line
    turnAngle(turnDirection == LEFT ? initialOffsetAngle : -initialOffsetAngle);

    // set velocity to continue turning
    setVelocity(0, turnDirection == LEFT ? angularVel : -angularVel);
;
    readLineSensors(); // readLineSensors to update lnLeft, lnMid, lnRight
    while ((lnLeft + lnRight == 0) && (elapsedTime < maxTime * 1000))
    {
        delay(10);
        elapsedTime += 10; // add on time elapsed
        readLineSensors(); // update line sensor variables to check in while loop
    }

    // stop ALBIE
    releaseMotors();
}

bool detectNode()
// returns true if node is detected
{
    // check if 3+ line sensors are on
    if (lnLeft + lnMid + lnRight + lnBack >= 3)
    // stop and check again, this stops false node detection
    {
        setVelocity(0, 0);
        releaseMotors();
        //followLine(lnJunctionVelocity, 0.0, lnFollowGain);
        delay(50);
        readLineSensors();

        
        if (lnLeft + lnMid + lnRight + lnBack >= 3)
        {
            if ((posWrtNode.lastNode == 2) && (posWrtNode.nextNode == 1))
            {
                moveDistance(10, lnFollowVelocity);
                readLineSensors();
                if (lnLeft + lnMid + lnRight + lnBack >= 3) return true;
                else return false;
            }
            else return true;
        }
    }

    return false;
}

bool followLineUntilNode(double maxRunTime, int velocity)
// maxRunTime in seconds
// returns true if node is reached, and false if maxRunTime is reached
// follows the line until a node is reached, or while loop runs for maxRunTime 
{
    // find latest time of finish using maxRunTime
    unsigned int elapsedTime = 0;

    // read line sensors
    readLineSensors();

    // set velocity to get started
    setVelocity(velocity, 0);

    // run until node detected, or maxRunTime has elapsed
    while ((!detectNode()) && (elapsedTime < maxRunTime * 1000))
    {
        // follow the line
        if ((posWrtNode.lastNode == 3) && (posWrtNode.nextNode == 2)) followLine(velocity, -0.3, lnFollowGainJunction);
        else if ((posWrtNode.lastNode == 4) && (posWrtNode.nextNode == 2)) followLine(velocity, 0.3, lnFollowGainJunction);
        else followLine(velocity, 0.0, lnFollowGain);
        delay(5);
        readLineSensors(); // need to update line sensor variables for while loop condition
        delay(5);
        elapsedTime += 10; // add time elapsed
    }

    // stop ALBIE
    releaseMotors();

    // return true if line has been reached
    if (elapsedTime < maxRunTime * 1000) return true;
    else return false;
}

void moveIntoStartingArea()
// moves ALBIE forward into starting area from node 1
{
    followLineForDistance(150, 0.0, 200);
}

void moveToDropPosition(int placementNode)
// moves the front of ALBIE to be above the drop position
{
    int reverseDist = -85;
    int backDist = -50;

    if (placementNode == 1)
    {
        // stores block between nodes 0 and 1
        navigateThroughNode(0);
        followLineForDistance(150, 0, 400);
    }
    else if (placementNode == 6)
    {
        // move to correct position
        moveDistance(backDist, 100);
        centerOnLine();
        followLineUntilNode(10, 100);
        moveDistance(-72, 100);

        turnSmallAngle(-6);
    }
    else if (placementNode == 5)
    {
        // move to correct position
        moveDistance(backDist, 100);
        centerOnLine();
        followLineUntilNode(10, 100);
        moveDistance(-80, 100);
        turnSmallAngle(2);
    }
    else
    {
        moveDistance(backDist, 100);
        centerOnLine();
        followLineUntilNode(10, 100);
        moveDistance(-60, 100);
    }
}

void moveAwayFromPlacedBlock(int placementNode)
// moves ALBIE away from block after a block has been placed
{
    if (placementNode == 6)
    {
        // move back 100mm
        moveDistance(-400, lnFollowVelocity);

        // turning on line skips node 5 and 7
        turnOnLine(LEFT, 10);
        //moveDistance(-100, 150);

        // update position
        posWrtNode.lastNode = 7;
        posWrtNode.nextNode = 1;
    }
    else if (placementNode == 5)
    {
        // move back 100mm
        moveDistance(-50, lnFollowVelocity);

        // turning on line skips node 7
        turnOnLine(LEFT, 10);

        // update position
        posWrtNode.lastNode = 7;
        posWrtNode.nextNode = 1;
    }
    else if (placementNode == 3)
    {
        // move backwards
        moveDistance(-50, lnFollowVelocity);

        // turn around and then set position
        if (posWrtNode.lastNode == 2)
        {
            turnOnLine(LEFT, 10);
            posWrtNode.lastNode = 3;
            posWrtNode.nextNode = 2;
        }
        else if (posWrtNode.lastNode == 4)
        {
            turnOnLine(RIGHT, 10);
            posWrtNode.lastNode = 3;
            posWrtNode.nextNode = 4;
        }
    }
    else if (placementNode == 4)
    {
        // move backwards
        moveDistance(-200, lnFollowVelocity);

        // turn around and then set position
        if (posWrtNode.lastNode == 2)
        {
            turnOnLine(RIGHT, 10);
            posWrtNode.lastNode = 4;
            posWrtNode.nextNode = 2;
        }
        else if (posWrtNode.lastNode == 3)
        {
            turnOnLine(RIGHT, 10);
            posWrtNode.lastNode = 4;
            posWrtNode.nextNode = 3;
        }
    }
    else if (placementNode == 1)
    {
        // move backwards
        moveDistance(-100, lnFollowVelocity);

        // turn around and then set position
        turnOnLine(RIGHT, 10);
        posWrtNode.lastNode = 1;
        posWrtNode.nextNode = 0;
    }
}

void placeBlock(int placementNode)
// places block once node has been reached
{
    // move block over drop position
    moveToDropPosition(placementNode);
    
    // drop block
    openArms(servoMaxOpenAngle);

    digitalWrite(blockMovingRedLedPin, LOW);
    digitalWrite(blockMovingBlueLedPin, LOW);

    moveAwayFromPlacedBlock(placementNode);
    closeArms(90);
    //calibrateColourSensor();
}

void pickupBlock(blockLinePosition blockPosition)
// sweeps line and picks up block
{
    openArms(servoMaxOpenAngle);

    if (((blockPosition == NODE23) || (blockPosition == NODE34)) || (blockPosition == NODE01))
    {
        leftServo.write(15);
        rightServo.write(135);
    }
    else if ((blockPosition == NODE24_1) || (blockPosition == NODE24_2))
    {
        leftServo.write(45);
        rightServo.write(165);
    }
    else 
    {
        leftServo.write(servoMaxOpenAngle);
        rightServo.write(90 + servoMaxOpenAngle);
    }

    int dist;

    // set search distance
    if (blockPosition == NODE01) dist = 500;
    else if (blockPosition == NODE23) dist = 480;
    else if (blockPosition == NODE34) dist = 600;
    else if (blockPosition == NODE24_1) dist = 500;
    else if (blockPosition == NODE24_2) dist = 1400;

    readLineSensors();
    followLineForDistance(150, 0, dist);

    // grab block
    leftServo.write(45);
    rightServo.write(135);
    closeArms(90);
    
    //move block in front of colour sensor
    shuffleForwards();
}



// navigation functions
bool nodeLink(int node1, int node2)
// returns true if there is a path between two nodes and false if not
{
    // node relation table, 1 is a link between nodes, 0 is no link
    // The nodes are: 
    // 0    Start area T-Junction
    // 1    Curved Junction
    // 2    T-Junction 1 (Central one on loop)
    // 3    Red 1 (Bottom Red)
    // 4    Red 2 (Top Red)
    // 5    Blue 1 (Bottom Blue)
    // 6    Blue 2 (Top Blue)
    // 7    T-Junction 2 (Between Curved Junciton and Blue 1)

    int nodeTable[numNodes][numNodes] =
    {
        {0, 1, 0, 0, 0, 0, 0, 0}, // Start Area
        {1, 0, 1, 0, 0, 0, 0, 1}, // Curved Junction
        {0, 1, 0, 1, 1, 0, 0, 0}, // T-Junction
        {0, 0, 1, 0, 1, 0, 0, 0}, // Red 1
        {0, 0, 1, 1, 0, 0, 0, 0}, // Red 2
        {0, 0, 0, 0, 0, 0, 1, 1}, // Blue 1
        {0, 0, 0, 0, 0, 1, 0, 0}, // Blue 2
        {0, 1, 0, 0, 0, 1, 0, 0}  // T-Junction 2
    };

    // now we need to remove links that a blocked by blocks
    int i, j;
    for (i = 0; i < 4; i++)
    {
        if (blockPlacedAtNode[i])
        // if a block is placed at the node, then set all paths to it as 0
        {
            for (j = 0; j < numNodes; j++)
            {
                // use i + 3 as blockPlacedAtNode[0] is for node 3 ect.
                nodeTable[i + 3][j] = 0;
                nodeTable[j][i + 3] = 0;
            }
        }
    }

    if (blockPositions[NODE23] == INITIAL)
    {
        nodeTable[2][3] = 0;
        nodeTable[3][2] = 0;
    }
    if (blockPositions[NODE34] == INITIAL)
    {
        nodeTable[3][4] = 0;
        nodeTable[4][3] = 0;
    }
    if ((blockPositions[NODE24_1] == INITIAL) || (blockPositions[NODE24_2] == INITIAL))
    {
        nodeTable[2][4] = 0;
        nodeTable[4][2] = 0;
    }

    return nodeTable[node1][node2];
}

numNodesIntArray findNodePath(int startNode, int endNode)
// finds the shortest node path between two nodes using a simplified version of Djikstra's algorithm
// returns a numNodesIntArray with nodes = {startNode, ..., endNode, -1, -1, .. -1}
// -1 is used as a null value for no node
{
    // array of shortest paths known to each node currently, use -1 for unused space
    numNodesIntArray shortestFoundPath[numNodes];
    numNodesIntArray dist, visited(0);
    int i, currentNode, node, depth = 0, minNode, minNodeDist;

    // set the first node to startNode
    for (node = 0; node < numNodes; node++) shortestFoundPath[node].nodes[0] = startNode;
    // set start node dist to 0
    dist.nodes[startNode] = 0;

    for (depth = 1; depth < numNodes; depth++)
    {
        // find closest node not visited
        minNode = -1;
        minNodeDist = numNodes;
        for (node = 0; node < numNodes; node++)
        {
            if (!visited.nodes[node])
            {
                if ((dist.nodes[node] != -1) && (dist.nodes[node] < minNodeDist))
                {
                    minNode = node;
                    minNodeDist = dist.nodes[node];
                }
            }
        }

        // set current node as the minNode found before
        currentNode = minNode;

        // mark node as visited
        visited.nodes[currentNode] = 1;

        // iterate over all nodes to check if they are connected to minNode
        for (node = 0; node < numNodes; node++)
        {
            // check if theres a path between nodes
            if (nodeLink(node, currentNode))
            {
                // check if node has not been visited before
                if (dist.nodes[node] == -1)
                {
                    // set the shortest path as the path through currentNode
                    // this will always be the shortest path as the nodes are all unit length from each other in this model
                    shortestFoundPath[node] = shortestFoundPath[currentNode];

                    // set next node in shortestFoundPath[node] to node
                    // use while loop to find the next node in shortestFoundPath
                    i = 0;
                    while ((shortestFoundPath[node].nodes[i] != -1) && (i < numNodes)) i++;

                    shortestFoundPath[node].nodes[i] = node;

                    // set the distance as depth and set as visited
                    dist.nodes[node] = depth;

                    // return the shortest path if the endNode has been reached
                    if (node == endNode)
                    {
                        return shortestFoundPath[node];
                    }
                }
            }
        }
    }

    // if function fails, return array of -1
    return numNodesIntArray(-1);
}

void navigateThroughNode(int nodeAfterJunction)
// handles the navigation through juctions, done case by case for each lastNode
{
    const double pullAwayTime = 1.2; // seconds, time that the bias remains on as ALBIE pulls away from the junction
    const int reverseDist = 20; // mm, distance ALBIE reverses for to get round T-Junctions 

    // set nodes from current position
    int lastNode = posWrtNode.lastNode, junctionNode = posWrtNode.nextNode;

    // update position
    posWrtNode.lastNode = junctionNode;
    posWrtNode.nextNode = nodeAfterJunction;

    // navigate through junction
    if (lastNode == -1)
    // case for starting area
    {
        if (junctionNode == 0)
        {
            if (nodeAfterJunction == 1) followLineForTime(lnJunctionVelocity, 0.0, pullAwayTime);
        }
    }
    else if (lastNode == 0)
    {
        if (junctionNode == 1)
        {
            if (nodeAfterJunction == 0) turnOnLine(RIGHT, 10);
            else if (nodeAfterJunction == 2) followLineForTime(lnJunctionVelocity, 0.1, pullAwayTime);
            else if (nodeAfterJunction == 7) turnOnLine(LEFT, 10);
        }
    }
    else if (lastNode == 1) 
    {
        if (junctionNode == 2)
        {
            if (nodeAfterJunction == 1)
            // can't just turn on line because you will hit tunnel
            {
                followLineForTime(lnJunctionVelocity, -0.8, pullAwayTime); // turn left
                followLineForTime(lnJunctionVelocity, 0, 600 / lnFollowVelocity); // move ~600 mm down line
                turnOnLine(RIGHT, 10); // turn around
                followLineUntilNode(10, lnFollowVelocity); // go back to node 2
                followLineForTime(lnJunctionVelocity, 0.8, pullAwayTime); // turn right into tunnel
            }
            else if (nodeAfterJunction == 3) 
            {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, 0.8, pullAwayTime); // turn right
            }
            else if (nodeAfterJunction == 4)
            {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, -0.8, pullAwayTime); // turn left
            }
        }
        else if (junctionNode == 7)
        {
            if (nodeAfterJunction == 1) turnOnLine(RIGHT, 10);
            else if (nodeAfterJunction == 5) {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, -0.95, pullAwayTime);
            }
        }
    }
    else if (lastNode == 2)
    {
        if (junctionNode == 1)
        {
            if (nodeAfterJunction == 0) followLineForTime(lnJunctionVelocity, -0.5, pullAwayTime); // turn left
            else if (nodeAfterJunction == 2) turnOnLine(RIGHT, 10); // turn around
            else if (nodeAfterJunction == 7) followLineForTime(lnJunctionVelocity, 0.8, pullAwayTime); // turn right
        }
        else if (junctionNode == 3)
        {
            if (nodeAfterJunction == 2) turnOnLine(LEFT, 10); // turn left
            else if (nodeAfterJunction == 4) followLineForTime(lnJunctionVelocity, 0.0, pullAwayTime); // go straight forwards
        }
        else if (junctionNode == 4)
        {
            if (nodeAfterJunction == 2) turnOnLine(RIGHT, 10); // turn right
            else if (nodeAfterJunction == 3) followLineForTime(lnJunctionVelocity, 0.0, pullAwayTime); // go straight forwards
        }
    }
    else if (lastNode == 3)
    {
        if (junctionNode == 2)
        {
            if (nodeAfterJunction == 1) 
            {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, -0.9, pullAwayTime); // turn left
            }
            else if (nodeAfterJunction == 3) turnOnLine(RIGHT, 10); // turn on line
            else if (nodeAfterJunction == 4) followLineForTime(lnJunctionVelocity, 0.5, pullAwayTime); // turn right
        }
    }
    else if (lastNode == 4)
    {
        if (junctionNode == 2)
        {
            if (nodeAfterJunction == 1)
            {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, 0.9, pullAwayTime); // turn right
            }
            else if (nodeAfterJunction == 3) followLineForTime(lnJunctionVelocity, -0.5, pullAwayTime); // turn left
            else if (nodeAfterJunction == 4) turnOnLine(LEFT, 10); // turn around
        }
        else if (junctionNode == 3)
        {
            if (nodeAfterJunction == 2) followLineForTime(lnJunctionVelocity, 0.0, pullAwayTime); // go straight ahead
            else if (nodeAfterJunction == 4) turnOnLine(RIGHT, 10); // turn around
        }
    }
    else if (lastNode == 5)
    {
        if (junctionNode == 6)
        {
            if (nodeAfterJunction == 5) 
            {
                moveDistance(80, lnFollowVelocity);
                turnOnLine(LEFT, 10);
                moveDistance(-50, lnFollowVelocity);
            }
        }
        else if (junctionNode == 7)
        {
            if (nodeAfterJunction == 1) {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, -0.1, pullAwayTime); // go forward
                turnOnLine(RIGHT, 10);
            }
            else if (nodeAfterJunction == 5) turnOnLine(LEFT, 10); // MIGHT BE TOO TIGHT A TURN
        }
    }
    else if (lastNode == 6)
    {
        if (junctionNode == 5)
        {
            if (nodeAfterJunction == 6) turnOnLine(LEFT, 10); // MIGHT BE TOO TIGHT A TURN
            else if (nodeAfterJunction == 7)
            {
                moveDistance(-reverseDist, lnFollowVelocity);
                followLineForTime(lnJunctionVelocity, -0.8, pullAwayTime); // turn left
            }
        }
    }
    else if (lastNode == 7)
    {
        if (junctionNode == 1)
        {
            if (nodeAfterJunction == 0)
            // go straight ahead along line 1-2, then turn on line to go down line 1-0
            {
                followLineForTime(lnJunctionVelocity, 0.0, pullAwayTime);
                turnOnLine(RIGHT, 10);
            }
            else if (nodeAfterJunction == 2) followLineForTime(lnJunctionVelocity, 0.0, pullAwayTime); // go straight ahead
            else if (nodeAfterJunction == 7) turnOnLine(LEFT, 10); // turn around
        }
        else if (junctionNode == 5)
        {
            if (nodeAfterJunction == 6) followLineForTime(lnJunctionVelocity, 0.8, pullAwayTime); // turn right
            else if (nodeAfterJunction == 7) turnOnLine(RIGHT, 10); // MIGHT BE TOO TIGHT
        }
    }
}

void navigateToNode(int destinationNode)
// navigates from current position to node
{
    int i;

    // find path
    numNodesIntArray nodePath = findNodePath(posWrtNode.nextNode, destinationNode);

    // follow line until we reach nextNode
    followLineUntilNode(100, lnFollowVelocity);

    // move between nodes in path
    for (i = 1; (i < numNodes) && (nodePath.nodes[i] != -1); i++)
    {
        delay(200);
        // navigate through the junction the node has just reached
        navigateThroughNode(nodePath.nodes[i]);

        // Now we are between nodePath.nodes[i - 1] & nodePath.node[i]
        // follow line until we reach nodePath.node[i]
        //if ((posWrtNode.lastNode == 3) && (posWrtNode.nextNode == 2)) followLineUntilNode(100, 80);
        followLineUntilNode(100, lnFollowVelocity);
        // Now we are at nodePath.node[i]
    }
}

blockLinePosition chooseNextBlock()
// choses the next block for ALBIE to pick up
{
    if (blockPositions[NODE23] == INITIAL) return NODE23;
    else if (blockPositions[NODE34] == INITIAL) return NODE34;
    else if (blockPositions[NODE23] == STORED) return NODE01; // if block NODE23 is in storage (between 0-1) then go to NODE01
    else if (blockPositions[NODE24_1] == INITIAL) return NODE24_1;
    else if (blockPositions[NODE24_1] == STORED) return NODE01; // if block NODE24 is in storage (between 0-1) then go to NODE01
    else if (blockPositions[NODE24_2] == INITIAL) return NODE24_2;
    else return ALLBLOCKSMOVED;
}

int chooseBlockPlaceLocation(blockLinePosition pickupLocation, blockCarryingStatus blockColour)
// returns the node the block should be placed on
{
    if (blockColour == BLUE)
    {
        // place at node 6 first, then node 5 if node 6 is full
        if (blockPlacedAtNode[3]) return 5;
        else return 6;
    }
    else if (pickupLocation == NODE01)
        // block must be red as it's not blue 
    {
        // return node 3 unless a block is already there, then place it at 4
        if (blockPlacedAtNode[0]) return 4;
        else return 3;
    }
    // if a red block is here then we need to store it on line 0-1 to avoid it getting in the way later
    else if (pickupLocation == NODE23) return 1;
    else if ((pickupLocation == NODE24_2) || (pickupLocation == NODE34))
    {
        // try to place the block at 4, but place at 3 if 4 is full
        if (blockPlacedAtNode[1]) return 3;
        else return 4;
    }
    else
    {
        // try to place the block at 3, but place at 4 if 3 is full
        if (blockPlacedAtNode[1]) return 4;
        else return 3;
    }
}

void navigateToBlock(blockLinePosition block)
// navigates to the start of the line the block is on
{
    if (block == NODE01) 
    {
        navigateToNode(1);
        navigateThroughNode(0);
    }
    else if (block == NODE23)
    {
        navigateToNode(2);
        delay(500);
        navigateThroughNode(3);
    }
    else if ((block == NODE24_1) || (block == NODE24_2))
    {
        navigateToNode(2);
        navigateThroughNode(4);
    }
    else if (block == NODE34)
    {
        navigateToNode(3);
        navigateThroughNode(4);
    }
}

void updateBlockStatusOnPickup(blockLinePosition blockPickupLocation)
// updates global variables on block pickup
{
    // for normal pickup nodes, set status to carrying
    if (blockPickupLocation != NODE01) blockPositions[blockPickupLocation] = CARRYING;
    else 
    // for node 1 where block is stored, find STORED block, then set that to carrying
    {
        for (int i = 0; i < 4; i++)
        {
            if (blockPositions[i] == STORED) blockPositions[i] = CARRYING;
        }
    }
}

void updateBlockStatusOnPlace(blockLinePosition blockPickupLocation, int placementNode)
// updates global variables on block place
{
    // check which block is being carried
    for (int i = 0; i < 4; i++)
    {
        if (blockPositions[i] == CARRYING)
        {
            // for normal nodes, set a reached TARGET and block placed at nod = true
            if (placementNode != 1)
            {
                blockPositions[i] = TARGET;
                blockPlacedAtNode[placementNode - 3] = true;
            }
            // for placing stoerd block at node 1, set position as STORED
            else  blockPositions[i] = STORED;
        }
    }
}



//setup and loop
void setup() 
{
// begin motor shield
AFMS.begin();

// setup pins
// line sensor pins
pinMode(lnLeftPin, INPUT);
pinMode(lnMidPin, INPUT);
pinMode(lnRightPin, INPUT);
pinMode(lnBackPin, INPUT);
// switch interrupt pin
attachInterrupt(digitalPinToInterrupt(switchPin), switchInterupt, FALLING);
// servo pins
leftServo.attach(leftServoPin);
rightServo.attach(rightServoPin);
// LED pins
pinMode(blockMovingAmberLedPin, OUTPUT);
pinMode(blockMovingBlueLedPin, OUTPUT);
pinMode(blockMovingRedLedPin, OUTPUT);
// colour sensor pins
pinMode(colourSenseBlueLed, OUTPUT);
pinMode(colourSenseRedLed, OUTPUT);
leftServo.write(90);
rightServo.write(90);

Serial.begin(9600);
}

void loop()
{
    blockCarryingStatus carriedBlockStatus;
    static blockLinePosition blockPickUpLocation;

    // check switch is on
    if (switchOn)
    {
        Serial.println("Start");
        // read line sensors
        readLineSensors();

        // read block colour
        carriedBlockStatus = readBlockColour();

        if ((carriedBlockStatus == RED) || (carriedBlockStatus == BLUE))
        {
            // turn on led when carrying block
            if (carriedBlockStatus == RED) digitalWrite(blockMovingRedLedPin, HIGH);
            else if (carriedBlockStatus == BLUE) digitalWrite(blockMovingBlueLedPin, HIGH);


            int placementNode;
            // choose placement node
            placementNode = chooseBlockPlaceLocation(blockPickUpLocation, carriedBlockStatus);

            // turn around unless lastNode is 3, this helps moving away from block
            if (((carriedBlockStatus == BLUE) || (blockPickUpLocation == NODE24_1)) || ((blockPickUpLocation == NODE23) || (blockPickUpLocation == NODE01)))
            {
                turnOnLine(RIGHT, 10);
                posWrtNode.moveNode(posWrtNode.lastNode);
            }

            // navigate to placement node, place block, update global variables
            if (placementNode == 5)
            {
                navigateToNode(7);
                moveDistance(lnDistFromAxle, 100);
                turnOnLine(LEFT, 10);
                followLineUntilNode(10, lnFollowVelocity);
            }
            else navigateToNode(placementNode);

            placeBlock(placementNode);
            updateBlockStatusOnPlace(blockPickUpLocation, placementNode);
        }
        else if (carriedBlockStatus == NO_BLOCK)
        {
            // chose next block
            blockPickUpLocation = chooseNextBlock();
            Serial.println(blockPickUpLocation);

            if (blockPickUpLocation == ALLBLOCKSMOVED)
            {
                // navigate back to start area and finish program
                navigateToNode(0);
                moveIntoStartingArea();
                switchOn = 0;
            }
            else
            {
                // navigate to line block is on, pickup block, update global variables
                navigateToBlock(blockPickUpLocation);
                pickupBlock(blockPickUpLocation);
                updateBlockStatusOnPickup(blockPickUpLocation);
            }
        }
    }
    else
    {
    }
}
